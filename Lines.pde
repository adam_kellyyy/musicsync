public class Lines extends Graphic {
    private int[][] lines = new int[16][height];
    private int linesRotate = 0;

    public Lines(){}

    public void render(String activeGraphics) {
        pushMatrix();
        strokeWeight(1);

        translate(0, 0, -500);
        translate(width/2,0,0);
        rotateY(linesRotate*PI/1000);
        translate(-width/2,0,0);
        

        linesRotate++;

        for (int i = 0; i < 16; i ++) {
            if (i%2 == 0) {
                lines[i][0] = -instrumentValues.get(i)/2;
            } else {
                lines[i][0] = instrumentValues.get(i)/2;
            }
        }

        for (int i = 0; i < 8; i ++) {
            for (int j = 0; j < height; j ++) {
                switch (i) {
                    case 0:
                        stroke(255,0,0);
                        fill(255,0,0);
                        break;
                    case 1:
                        stroke(255,150,0);
                        fill(255,150,0);
                        break;
                    case 2:
                        stroke(255,255,0);
                        fill(255,255,0);
                        break;
                    case 3:
                        stroke(0,255,0);
                        fill(0,255,0);
                        break;
                    case 4:
                        stroke(0,0,255);
                        fill(0,0,255);
                        break;
                    case 5:
                        stroke(153,50,204);
                        fill(153,50,204);
                        break;
                    case 6:
                        stroke(238,130,238);
                        fill(238,130,238);
                        break;
                    case 7:
                        stroke(255,255,255);
                        fill(255,255,255);
                        break;
                    
                }
            
                pushMatrix();
                translate(((width*(i)/8) + (width/16) + lines[i*2][j] + (width*(i)/8) + (width/16) + lines[(i*2)+1][j] + 4)/2, j, 0);
                rotateY(-linesRotate*PI/1000);
                rect(lines[(i*2)+1][j], 0, lines[i*2][j]-lines[(i*2)+1][j], 1);
                popMatrix();        
                
            }


            
        }

        for (int i = 0; i < 16;  i ++) {
            for (int j = height-1; j > 0; j --) {
                lines[i][j] = lines[i][j-1];
            }
        }

        for (int i = 0; i < 16; i ++) {
            lines[i][0] = 0;
        }

        popMatrix();

        
    }



}