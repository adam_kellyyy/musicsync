public class Landscape extends Graphic {

    private int cols, rows;
    private int scl;
    private float[][] alt;
    private color[] landscapeColor;
    private boolean landScapeColourSwitch = false;
    private float flying = 0;
    private int maxShift = 4;
    private int shift = 5;
    private int scale = 30;
    private int red = 0;
    private int green = 255;
    private int blue = 0;
    private int fade = 20;

    public Landscape () {
        scl = scale;
        cols = width/scl;
        rows = height/scl;
        rows = rows*2;
        alt = new float[rows][cols];
        landscapeColor = new color[rows];
        for (int i = 0; i < rows; i ++) {
            landscapeColor[i] = color(red, green, blue);
        }
    }

    public void render(String activeGraphics) {

        pushMatrix();
        

        noFill();

        strokeWeight(3);


        if (notes[0] != 0 && !landScapeColourSwitch) {
            landScapeColourSwitch = true;
        } else if (notes[0] == 0) {
            landScapeColourSwitch = false;
        }
    
        shift++;
        int ccAvg = 0;
        if (shift > maxShift) {
            shift = 0;

            for (int y = rows-1; y >=1;  y --) {
                System.arraycopy(alt[y-1], 0, alt[y], 0, cols);
                landscapeColor[y] = landscapeColor[y-1];
            }
            flying += 4;
            
            for (int i = 0; i < cols; i ++) {
                
                int numCc = 1;
                for (int j = 0; j < 16; j ++) {
                    if (freqBandValues.get(j) > 0) {
                        ccAvg += freqBandValues.get(j);
                        numCc ++;
                    }
                }
                ccAvg = ccAvg/numCc;
                alt[0][i] = map(ccAvg*noise(flying/6, (float)i/6), 0, 128, 0, 300)*2;
                

                landscapeColor[0] = color(red, green, blue);
            }
        }
        

        translate(width/2, height/2);
        rotateX(PI/3);
        translate(-width/2, -height/2);
        
       

        for (int y = 0; y < rows-1;  y ++) {

            if (y < fade) {
                stroke((red(landscapeColor[y])/fade)*y, (green(landscapeColor[y])/fade)*y, (blue(landscapeColor[y])/fade)*y);
            } else {
                stroke(landscapeColor[y]);
            }
            
            beginShape(TRIANGLE_STRIP);
            for (int x = 0; x < cols; x ++) {
                if (x%10 == 0) {
                    vertex(x*scl, (y*scl)+scl*(shift/(float)maxShift), random(alt[y][x], alt[y][x]));
                    vertex(x*scl, ((y+1)*scl)+scl*(shift/(float)maxShift), random(alt[y+1][x], alt[y+1][x]));
                } else {
                    vertex(x*scl, (y*scl)+scl*(shift/(float)maxShift), alt[y][x]);
                    vertex(x*scl, ((y+1)*scl)+scl*(shift/(float)maxShift), alt[y+1][x]);
                }
            }
            endShape();
        }
        popMatrix();

    }

    public void setRed (int val) {
        red = val;
    }

    public void setGreen (int val) {
        green = val;
    }

    public void setBlue (int val) {
        blue = val;
    }

}
