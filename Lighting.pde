public class Lighting {

    private ArrayList<Integer> driver;

    private int channel = 0;
    private boolean manualControl = false;
    private int autoRed = 127;
    private int autoGreen = 127;
    private int autoBlue = 127;
    private boolean preview = false;
    private int currentRed = 0;
    private int currentGreen = 0;
    private int currentBlue = 0;

    public Lighting(ArrayList<Integer> driver) {
        this.driver = driver;
    }

    public void update() {
        if (!manualControl) {
            setRed((int)((driver.get(channel*2) + driver.get((channel*2) + 1))*(autoRed/255.0)));
            setGreen((int)((driver.get(channel*2) + driver.get((channel*2) + 1))*(autoGreen/255.0)));
            setBlue((int)((driver.get(channel*2) + driver.get((channel*2) + 1))*(autoBlue/255.0)));
        }

        if (preview) {
            stroke(currentRed*2, currentGreen*2, currentBlue*2);
            fill(currentRed*2, currentGreen*2, currentBlue*2);
            rect(0, 0, 100, 100);
        }

    }

    public void setDriver(ArrayList<Integer> driver) {
        this.driver = driver;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    private void setRed(int brightness) {
        myBus.sendControllerChange(0, 0, brightness);
        currentRed = brightness;
    }

    private void setGreen(int brightness) {
        myBus.sendControllerChange(2, 0, brightness);
        currentGreen = brightness;
    }
    
    private void setBlue(int brightness) {
        myBus.sendControllerChange(1, 0, brightness);
        currentBlue = brightness;
    }

    private void setAutomaticRed(int brightness) {
        autoRed = brightness;
    }

    private void setAutomaticGreen(int brightness) {
        autoGreen = brightness;
    }
    
    private void setAutomaticBlue(int brightness) {
        autoBlue = brightness;
    }

    

    private void setManualControl(boolean manualControl) {
        this.manualControl = manualControl;
    }

    public void setPreview(boolean val) {
        preview = val;
    }

}