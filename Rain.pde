public class Rain extends Graphic {
    private int rainH = height;
    private int rainW = width;
    private int maxDropsPerFrame = 5;

    private ArrayList<Raindrop> raindrops = new ArrayList<>();


    private class Raindrop {
        private int size = 0;
        private int x = 0;
        private int y = 0;
        private color fill;
        private float h = 0;
        private boolean hasFill;
        
        public Raindrop(int size, int x, int y, color fill, float h, boolean hasFill) {
            this.size = size;
            this.x = x;
            this.y = y;
            this.fill = fill;
            this.h = h;
            this.hasFill = hasFill;
        }

        public void draw() {
            stroke(fill);
            if (hasFill) {
                fill(fill);
            } else {
                noFill();
            }
            pushMatrix();
            translate(0, 0, h);
            circle (x*(width/rainW), y*(height/rainH), size);
            popMatrix();
        }

        public void update() {
            size = size - 1;
        }

        public int getSize() {
            return size;
        }

    }

    public void render(String activeGraphics) {

        noFill();
        strokeWeight(2);
        pushMatrix();

        if (activeGraphics.contains("levels")) {

            rotateX(-PI/8 + PI/2);
            translate(0, 0, -776);
            rotateZ(-PI/4);
        }

        if (cameraAngle == 1) {
            translate(0, 100, 0);
            rotateX(PI/3.5);
        }
        int cctotal = 0;
        for (int i = 0; i < 16; i ++) {
            cctotal += freqBandValues.get(i);
        }

        cctotal = cctotal/16;

        for (int i = 0; i < maxDropsPerFrame; i ++) {
            if (random(0, 255) + cctotal > 255) {
                int y = (int)random(0, rainH);
                int x = (int)random(0, rainW);
                color c = color((int)random(0, 200), 0, (int)random(150, 255));
                if (random(0, 10) < 5) {
                    raindrops.add(new Raindrop((int)random(0, 200), x, y, c, random(0, 1), true));
                } else {
                    raindrops.add(new Raindrop((int)random(0, 200), x, y, c, random(0, 1), false));
                }
            }
        }

        ArrayList<Raindrop> removals = new ArrayList<>();

        for (Raindrop drop : raindrops) {
            drop.draw();
            drop.update();
            if (drop.getSize() < 1) {
                removals.add(drop);
            }
        }

        for (Raindrop drop : removals) {
            raindrops.remove(drop);
        }
        
        popMatrix();

        }
}
