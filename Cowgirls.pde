public class Cowgirls extends Graphic {

    private ShootingStars stars = new ShootingStars(2, 0.001, 100, 2);
    private Levels levels = new Levels();

    private Lighting lighting;

    private PShape mainHat;
    private PShape smallHat;
    private PShape girlStupidOutline;
    private PShape girlStupidMain;
    private float rotate;
    private PImage hatTexture = loadImage("cowgirlWhite.png");
    private PShape globe;
    private int originalHatPixels[];
    private int scene = 0;
    private color graphicColors[] = {
        color(255, 0, 255),
        color(91, 223, 230),
        color(255, 0, 0),
        color(0, 255, 0),
        color(252, 247, 18),
        color(0, 0, 255)
    };
    private int graphicColorInd = 0;

    public Cowgirls (Lighting lighting) {

        this.lighting = lighting;

        lighting.setDriver(freqBandPeak);
        lighting.setChannel(0);

        mainHat = loadShape("cowgirl.obj");
        smallHat = loadShape("cowgirl.obj");
        girlStupidMain = loadShape("girlstupidMain.obj");
        girlStupidOutline = loadShape("girlstupidOutline.obj");

        mainHat.rotateX(PI);            
        mainHat.scale(180);
        smallHat.rotateX(PI);            
        smallHat.scale(60);

        girlStupidMain.rotateX(PI);            
        girlStupidOutline.rotateX(PI);            

        girlStupidOutline.scale(800);
        girlStupidMain.scale(800);


        mainHat.rotateX(-0.4);
        smallHat.rotateX(-0.4);
        girlStupidMain.rotateZ(-0.5);
        girlStupidOutline.rotateZ(-0.5);
        

        hatTexture.loadPixels();
        originalHatPixels = new int[hatTexture.pixels.length];
        for (int i = 0; i < hatTexture.pixels.length; i ++) {
            originalHatPixels[i] = hatTexture.pixels[i];
        }

        cycleColorUp();
        cycleColorDown();

    }

    public void render(String activeGraphics) {

        rotate += 0.01;
        if (rotate > 2*PI) {
            rotate = 0;
        }

        float brightnessA = 0;

        for (int i = 0; i < 4; i ++) {
            brightnessA += freqBandValues.get(i);
        }

        brightnessA = brightnessA/2;

        float brightnessB = 0;

        for (int i = 12; i < 16; i ++) {
            brightnessB += freqBandValues.get(i);
        }

        brightnessB = brightnessB*1.6;

        if (scene != 2) {

            hatTexture.loadPixels();
            for (int i = 0; i < hatTexture.pixels.length; i ++) {

                float brightness = brightnessA/255.0;

                float value = red(originalHatPixels[i])/255.0;

                if (i < hatTexture.pixels.length/2) {
                    // brightness = brightnessB/255.0;
                }

                if (originalHatPixels[i] != BLACK) {
                    hatTexture.pixels[i] = color(
                        red(graphicColors[graphicColorInd])*value*brightness,
                        green(graphicColors[graphicColorInd])*value*brightness,
                        blue(graphicColors[graphicColorInd])*value*brightness);
                } else {
                    hatTexture.pixels[i] = color(
                        red(graphicColors[graphicColorInd])*0.05,
                        green(graphicColors[graphicColorInd])*0.05,
                        blue(graphicColors[graphicColorInd])*0.05);
                }

            }

            hatTexture.updatePixels();

            mainHat.setTexture(hatTexture);
            smallHat.setTexture(hatTexture);

        } else {
            girlStupidOutline.setFill(
                color(
                    red(graphicColors[graphicColorInd])*brightnessA/255.0,
                    green(graphicColors[graphicColorInd])*brightnessA/255.0,
                    blue(graphicColors[graphicColorInd])*brightnessA/255.0
                )
            );

            girlStupidMain.setFill(
                color(
                    red(graphicColors[graphicColorInd])*0.05,
                    green(graphicColors[graphicColorInd])*0.05,
                    blue(graphicColors[graphicColorInd])*0.05
                )
            );
        }
        

        if (scene == 0) {

            pushMatrix();
            translate(width/2, (height/2)-90, 0);

            pushMatrix();
            rotateY(rotate);

            shape(mainHat);
            popMatrix();   

            popMatrix();


        } else if (scene == 1) {

            for (int i = 0; i < 4; i ++) {
                for (int j = 0; j < 4; j ++) {

                    int x = (i*width/4) + ((width/4)/2);
                    int y = (j*height/4) + ((height/4)/2);

                    pushMatrix();

                    translate(x, y, 0);
                    rotateY(rotate);

                    shape(smallHat);
                    popMatrix();
                }
            }
        } else if (scene == 2) {
            pushMatrix();
            translate(width/2, height/2, 0);

            pushMatrix();
            translate(0, 0, -2000);
            rotateY(rotate);

            shape(girlStupidMain);
            shape(girlStupidOutline);
            popMatrix();   

            popMatrix();
        }

    
        pushMatrix();

        translate(0, 0, -6000);
        rotateX(PI/2);
        stars.render(null);
        
        
        popMatrix();

    }

    public void cycleSceneUp() {
        scene ++;

        if (scene > 2) {
            scene = 0;
        }
    }

    public void cycleSceneDown() {
        scene --;

        if (scene < 0) {
            scene = 2;
        }
    }

    public void cycleColorUp() {
        graphicColorInd ++;

        if (graphicColorInd >= graphicColors.length) {
            graphicColorInd = 0;
        }

        lighting.setAutomaticRed((int)(red(graphicColors[graphicColorInd])*0.5));
        lighting.setAutomaticGreen((int)(green(graphicColors[graphicColorInd])*0.5));
        lighting.setAutomaticBlue((int)(blue(graphicColors[graphicColorInd])*0.5));
    }

    public void cycleColorDown() {
        graphicColorInd --;

        if (graphicColorInd < 0) {
            graphicColorInd = graphicColors.length -1;
        }

        lighting.setAutomaticRed((int)(red(graphicColors[graphicColorInd])*0.5));
        lighting.setAutomaticGreen((int)(green(graphicColors[graphicColorInd])*0.5));
        lighting.setAutomaticBlue((int)(blue(graphicColors[graphicColorInd])*0.5));
    }



}