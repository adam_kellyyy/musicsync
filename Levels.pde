public class Levels extends Graphic {

    private int barHeight = 1000;
    private int barWidth = 200;

    public void render(String activeGraphics) {
        
        noStroke();
        noFill();

        pushMatrix();

        translate(200, 300+height/2, 0);
        rotateX(-PI/8);
        rotateY(PI/5);

        pushMatrix();


        fill(50,0,0);


        for (int i=0; i<16; i++) {
            // get the h and multiply by 4 to increase the visibility
            float h = freqBandPeak.get(i) * barHeight/255.0;
            // draw a rect
            rect(i*barWidth, 0, barWidth, h);        
        }

        popMatrix();

        fill(255,0,0);

        pushMatrix();


        rotateX(PI);

        for (int i=0; i<16; i++) {
            // get the h and multiply by 4 to increase the visibility
            float h = freqBandPeak.get(i) * barHeight/255.0;
            // draw a rect
            rect(i*barWidth, 0, barWidth, h);        
        }

        popMatrix();
        popMatrix();


    }

}