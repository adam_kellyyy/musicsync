public class Lyrics {
    private int songIndex = 0;
    private int lineIndex = 0;
    private int wordIndex = 0;

    // songs -> lines -> words
    private ArrayList<ArrayList<ArrayList<String>>> lyrics = new ArrayList<>();

    public Lyrics() {
        String folderString = System.getProperty("user.home") + "\\Documents\\Music Making\\lyrics";
        System.out.println(folderString);
        File folder = new File(folderString);
        File[] listOfFiles = folder.listFiles();

        int i = 0;
        for (File file : listOfFiles) {
            if (file.isFile() && !file.getName().contains(".bak")) {
                lyrics.add(new ArrayList<>());
                String[] lines = loadStrings(folderString + "\\" + file.getName());

                int j = 0;
                for (String line : lines) {
                    if (line.length() == 0) {
                        continue;
                    }

                    // for a space without splitting a word, use _
                    // for a new line in the graphic, use \t

                    lyrics.get(i).add(new ArrayList<>());
                    String[] words = line.split("[ \n]");
                    for (String word : words) {
                        word = word.replaceAll(" ","");
                        word = word.replaceAll("_"," ");
                        word = word.replaceAll("\t","\n");
                        lyrics.get(i).get(j).add(word);
                    }
                    j ++;
                }

                i ++;

            }
        }

    }

    public void render() {
        pushMatrix();
        translate(0, -100, 0);
        textAlign(CENTER, CENTER);
        fill(255);
        text(lyric, width/2, height/2);
        noStroke();
        popMatrix();
    }

    public void loadSong(int song) {
        if (song < lyrics.size()) {
            songIndex = song;
        }
        lineIndex = 0;
        wordIndex = 0;
    }

    public String noteOn() {
        if (songIndex < lyrics.size() 
            && lineIndex < lyrics.get(songIndex).size() 
            && wordIndex < lyrics.get(songIndex).get(lineIndex).size()) {

            String ret = lyrics.get(songIndex).get(lineIndex).get(wordIndex);
            wordIndex ++;
            return ret;

        } else {
            return "";
        }

    }

    public boolean noteOff() {
        // if its the last word in a line and the note is released,
        // we remove the word from the screen, otherwise do nothing

        if (wordIndex == lyrics.get(songIndex).get(lineIndex).size()) {
            wordIndex = 0;
            lineIndex ++;
            if (lineIndex == lyrics.get(songIndex).size()) {
                lineIndex = 0;
            }

            return true;
        }

        return false;

    }

}