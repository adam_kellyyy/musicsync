public class FloatingBalls extends Graphic {

    private ShootingStars stars = new ShootingStars(2, 0.001, 100, 2);

    private float rotation = 0;
    private float rotationRate = 0.5;
    
    public void render(String activeGraphics) {



        pushMatrix();

        pushMatrix();
        translate(0, 0, -6000);
        rotateX(PI/2);
        stars.render(null);
        
        popMatrix();

        popMatrix();

        pushMatrix();
                
        translate(width/2, 0, -300);

        // rotateY(rotation);



        for (int i = 0; i < 8; i ++) {
            stroke(getColor(i));
            noFill();
            strokeWeight(1);


            PShape b;
            pushMatrix();
            int x = (-width/2 + (width/8)*i)+(width/16);
            int y = height/2;

            translate(x, y, 0);


            


            float value = (instrumentPeak.get(i*2) + instrumentPeak.get((i*2)+1))/2;
            
            b = createShape(SPHERE, value*0.8);
            shape(b);
            popMatrix();

        }

        rotation += rotationRate/100;

        if (rotation > 2*PI) {
            rotation = 0;
        }

        popMatrix();


    }


   

}