public class Orbit extends Graphic {

    // TODO maybe make balls object oriented for readability
    private float speed = 0.2;
    private float[] rotations = {0, 0, 0, 0, 0, 0, 0, 0};
    private float[] speeds = {0.021, 0.011, 0.016, 0.030, 0.017, 0.028, 0.026, 0.013};
    private float[][] sizes = {{0, 0, 0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0, 0, 0}};

    private int[] distances = {300, 370, 500, 620, 680, 740, 840, 960};
    private float sunRotation = 0.0;
    private float sunSpeed = 0.01;

    private ShootingStars stars = new ShootingStars(2, 0.001, 100, 2);


    public Orbit() {
        for (int i = 0; i < 8; i ++) {
            rotations[i] = random(0, 100);
        }
    }

    public void render(String activeGraphics) {

        pushMatrix();

        translate(0, 0, -6000);
        rotateX(PI/2);
        stars.render(null);
        
        popMatrix();

        strokeWeight(0.5);

        for (int i = 0; i < 2; i ++) {
            for (int j = 0; j < 8; j ++) {
                sizes[2-i][j] = sizes[2-i-1][j];
            }
        }

        for (int i = 0; i < 8; i ++) {
            sizes[0][i] = 5+((freqBandValues.get(i*2) + freqBandValues.get((i*2)+1))/2)/3;
        }

        for (int i = 0; i < 8; i ++) {
            rotations[i] += speeds[i]*speed;
            if (rotations[i] > 2*PI) {
                rotations[i] -=2*PI;
            }
        }
        sunRotation += sunSpeed*speed;
        if (sunRotation > 2*PI) {
            sunRotation -=2*PI;
        }

        pushMatrix();
        translate(width/2, height/2, 0);
        noFill();

        stroke(255, 100, 100);
        noFill();

        pushMatrix();
        rotateY(sunRotation);
        sphere(75);
        popMatrix();


        for (int i = 0; i < 8; i ++) {
            noFill();
            stroke(255, 100, 100);
            pushMatrix();
            translate(0, 0, -1);
            circle(0, 0, distances[i]);
            popMatrix();

            pushMatrix();
            stroke(255);
            fill(getColor(i));
            rotateZ(rotations[i]);
            circle(distances[i]/2, 0, (sizes[0][i]+sizes[1][i]+sizes[2][i]/3));
            popMatrix();
        }






        popMatrix();
    }

}