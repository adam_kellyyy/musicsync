import themidibus.*; //Import the library
import java.lang.*;
import java.util.ArrayList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

private boolean grid = false;
int numGraphics = 0;
boolean test = false;
boolean lightingPreview = false;
boolean printFPS = false;

private int smooth = 2;
private int peakReleaseLength = 0;
private int peakResetSpeed = 5;

ArrayList<Integer> freqBandValues = new ArrayList<Integer>();
ArrayList<Integer> freqBandAvg = new ArrayList<Integer>();
ArrayList<Integer> freqBandPeak = new ArrayList<Integer>();
ArrayList<Integer> freqBandPeakRelease = new ArrayList<Integer>();

ArrayList<Integer> instrumentValues = new ArrayList<Integer>();
ArrayList<Integer> instrumentAvg = new ArrayList<Integer>();
ArrayList<Integer> instrumentPeak = new ArrayList<Integer>();
ArrayList<Integer> instrumentPeakRelease = new ArrayList<Integer>();

private int[][] freqBandBars = new int[smooth][16];
private int[][] instrumentBars = new int[smooth][16];

private Lighting lighting;

private boolean lyricsOnly = false;

private boolean inputFound = false;
private boolean outputFound = false;
private Cowgirls cowgirls;

/*
0: Floating balls
1: Landscape
2: Levels
3: Game of Life
4: Rubix
5: Rain
6: Lines
7: Speakers
8: Spectrum
9: Orbit
10: City
11: Waves
12: ToyStory
13: Pinball
*/

SceneManager sceneManager = new SceneManager();

void keyPressed() {
    if (key == CODED) {
        if (keyCode == UP) {
            // sceneManager.keyNext();
            cowgirls.cycleColorUp();
        } else if (keyCode == DOWN) {
            // sceneManager.keyPrevious();
            cowgirls.cycleColorDown();
        } else if (keyCode == RIGHT) {
            cowgirls.cycleSceneUp();
        } else if (keyCode == LEFT) {
            cowgirls.cycleSceneDown();
        }

    } else {
        if (key == ' ') {
            // sceneManager.toggleCurrent();

            // System.out.println(sceneManager);

            // System.out.println();
            // cowgirls.cycleScene();

        }
    }

}

color BLACK = color(0, 0, 0);
color WHITE = color(255, 255, 255);
MidiBus myBus; // The MidiBus
int[] notes = new int[128];
color currentColor = WHITE;
int cameraAngle = 0;
String[] midiInput;
String lyric = "";
Lyrics lyrics;

int tick = 0;

void setup() {
    fullScreen(P3D, 2);
    background(0);
    loadPixels();

    lyrics = new Lyrics();

    lighting = new Lighting(instrumentPeak);
    lighting.setPreview(lightingPreview);

    // myBus = new MidiBus();
    // for (int i = 0; i < myBus.availableInputs().length; i ++) {
    //     System.out.println(myBus.availableInputs()[i]);
    // }



    myBus = new MidiBus(this);
    // myBus.list();

    inputFound = myBus.addInput("loopMIDI Port");
    outputFound = myBus.addOutput("Teensy MIDI");

    smooth(8);
    frameRate(60);

    for (int i = 0; i < 16; i ++) {
        freqBandValues.add(0);
        freqBandAvg.add(0);
        freqBandPeak.add(0);
        freqBandPeakRelease.add(0);

        instrumentValues.add(0);
        instrumentAvg.add(0);
        instrumentPeak.add(0);
        instrumentPeakRelease.add(0);

    }
    
    Graphic g;

    sceneManager.add(new FloatingBalls());
    sceneManager.add(new Landscape());
    sceneManager.add(new Levels());
    sceneManager.add(new GameOfLife());
    sceneManager.add(new Rubix());
    sceneManager.add(new Rain());
    sceneManager.add(new Lines());
    sceneManager.add(new Spectrum());
    sceneManager.add(new Speakers());
    sceneManager.add(new Orbit());
    sceneManager.add(new City());
    sceneManager.add(new Waves());
    // sceneManager.add(new ToyStory());
    // sceneManager.add(new Pinball());
    cowgirls = new Cowgirls(lighting);
    sceneManager.add(cowgirls, true);

    if (test) {
        midiInput = loadStrings("midiValues.txt");
    }

    textMode(SHAPE);
    textAlign(LEFT, CENTER);
    textFont(createFont("ITC Avant Garde Gothic Bold.otf", 300));

}

void draw() {


    for (int i = 0; i < 16; i ++) {
            freqBandAvg.set(i, 0);
            instrumentAvg.set(i, 0);
    }

    for (int i = smooth-1; i >=1;  i --) {
        System.arraycopy(freqBandBars[i-1], 0, freqBandBars[i], 0, 16);
        System.arraycopy(instrumentBars[i-1], 0, instrumentBars[i], 0, 16);
    }

    for (int i = 0; i < 16; i ++) {
        freqBandBars[0][i] = freqBandValues.get(i);
        instrumentBars[0][i] = instrumentValues.get(i);
        // System.out.print(String.format("%03d", freqBandValues.get(i)));
        // System.out.print("   ");
    }

    // System.out.println();

    for (int i = 0; i < 16; i ++) {
        for (int j = 0; j < smooth; j ++) {
            freqBandAvg.set(i, freqBandAvg.get(i) + freqBandBars[j][i]);
            instrumentAvg.set(i, instrumentAvg.get(i) + instrumentBars[j][i]);
        }
        freqBandAvg.set(i, freqBandAvg.get(i)/smooth);
        instrumentAvg.set(i, instrumentAvg.get(i)/smooth);
    }

    for (int i = 0; i < 16; i ++) {
        if (freqBandValues.get(i) >= freqBandPeak.get(i)) {
            freqBandPeak.set(i, freqBandValues.get(i));
            freqBandPeakRelease.set(i, peakReleaseLength);
        } else if (freqBandPeakRelease.get(i) > 0) {
            freqBandPeakRelease.set(i, freqBandPeakRelease.get(i) - 1);
        } else {
            freqBandPeak.set(i, freqBandPeak.get(i) - peakResetSpeed);
            if (freqBandValues.get(i) > freqBandPeak.get(i)) {
                freqBandPeak.set(i, freqBandValues.get(i));
            }
        }
    }

    for (int i = 0; i < 16; i ++) {
        if (instrumentValues.get(i) >= instrumentPeak.get(i)) {
            instrumentPeak.set(i, instrumentValues.get(i));
            instrumentPeakRelease.set(i, peakReleaseLength);
        } else if (instrumentPeakRelease.get(i) > 0) {
            instrumentPeakRelease.set(i, instrumentPeakRelease.get(i) - 1);
        } else {
            instrumentPeak.set(i, instrumentPeak.get(i) - peakResetSpeed);
            if (instrumentValues.get(i) > instrumentPeak.get(i)) {
                instrumentPeak.set(i, instrumentValues.get(i));
            }
        }
    }

    String[] values;
    
    if (test) {
        values = midiInput[tick].split(",");

        for (int i = 0; i < 16; i ++) {
            freqBandValues.set(i, Integer.parseInt(values[i]));
            instrumentValues.set(i, Integer.parseInt(values[i]));
        }

        tick++;
    }
    if (printFPS) {
        System.out.println(frameRate);
    }

    // background(0);

    sceneManager.render();

    lighting.update();

    if (grid) {
        stroke(255);
        for (int i = 1; i < 3; i++) {
            line ((width*i)/3.0, 0, (width*i)/3.0, height);
            line (0, (height*i)/3.0, width, (height*i)/3.0);
        }
    }

    if (sceneManager.hasActive() && lyricsOnly) {
        textFont(createFont("ITC Avant Garde Gothic Bold.otf", 300));
        lyricsOnly = false;
    } else if (!sceneManager.hasActive() && !lyricsOnly) {
        textFont(createFont("ITC Avant Garde Gothic Bold.otf", 700));
        lyricsOnly = true;
    }
    lyrics.render();

}

color getColor(int i) {
    switch (i) {
        case 0:
            return color(255,0,0);
            
        case 1:
            return color(255,150,0);
            
        case 2:
            return color(255,255,0);
            
        case 3:
            return color(0,255,0);
            
        case 4:
            return color(0,0,255);
            
        case 5:
            return color(153,50,204);
            
        case 6:
            return color(238,130,238);
            
        default:
            return color(255,255,255);
    }
}

void noteOn(int channel, int pitch, int velocity) {

    // channel 0 is scene changes
    // channel 1 is lyrics triggering
    // channel 2 is switch for manual vs automatic lighting
    // channel 3 is what driver automatic lighting listens to

    if (channel == 0) {
        if (velocity == 1) {
            sceneManager.setScene(pitch, false);
        } else if (velocity == 127) {
            sceneManager.setScene(pitch, true);
        }
    } else if (channel == 1) {
        if (pitch == 0) {
            lyrics.loadSong(velocity-1);
        } else {
            lyric = lyrics.noteOn();
        }
    } else if (channel == 2) {
        if (pitch == 0) {
            lighting.setManualControl(true);
        } else {
            lighting.setManualControl(false);
        }
    } else if (channel == 3) {
        if (pitch == 0) {
            lighting.setDriver(freqBandPeak);
        } else {
            lighting.setDriver(instrumentPeak);
        }
    }

}

void noteOff(int channel, int pitch, int velocity) {

    if (channel == 0) {
        notes[pitch] = 0;
    } else if (channel == 1) {
        if (pitch != 0) {
            boolean ret = lyrics.noteOff();
            if (ret) {
                lyric = "";
            }
        }
    }

}

void controllerChange(int channel, int number, int value) {

    // channel 0 is frequency bands
    // channel 1 is instruments
    // channels 2, 3, 4 are manual lighting RGB values
    // channels 5, 6, 7 are automatic lighting RGB values
    // channel 8 is what instrument/band automatic lighting listens to
    // channels 9, 10, 11 are graphic colours


    if (instrumentValues.size() == 16 && number < 16) {
        if (channel == 0) {
            freqBandValues.set(number, value);
        } else if (channel == 1) {
            instrumentValues.set(number, value);
        } else if (channel == 2) {
            // RED
            lighting.setRed(value*2);
        } else if (channel == 3) {
            // GREEN
            lighting.setGreen(value*2);
        } else if (channel == 4) {
            // BLUE
            lighting.setBlue(value*2);
        } else if (channel == 5) {
            // RED
            lighting.setAutomaticRed(value*2);
        } else if (channel == 6) {
            // GREEN
            lighting.setAutomaticGreen(value*2);
        } else if (channel == 7) {
            // BLUE
            lighting.setAutomaticBlue(value*2);
        } else if (channel == 8) {
            if (value < 8) {
                lighting.setChannel(value);
            }
        } else if (channel == 9) {
            sceneManager.setRed(value*2);
        } else if (channel == 10) {
            sceneManager.setGreen(value*2);
        } else if (channel == 11) {
            sceneManager.setBlue(value*2);
        }
    }

}

void reset() {
    noFill();
    noStroke();
}
