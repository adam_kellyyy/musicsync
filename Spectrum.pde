public class Spectrum extends Graphic {
    ArrayList<ArrayList<Integer>> spectrum = new ArrayList();

    private int speed = 5;
    private float h = 0.5;

    public Spectrum() {
        for (int i = 0; i < 16; i ++) {
            ArrayList<Integer> temp = new ArrayList();
            for (int j = 0; j < (width/2)/speed; j ++) {
                temp.add(0);
            }
            spectrum.add(temp);
        }

    }

    public void render(String activeGraphics) {
        strokeWeight(4);
        
        for (int i = 0; i < 16; i ++) {
            if (spectrum.get(i).size() > (width/2)/speed) {
                spectrum.get(i).remove((width/2)/speed);
            }

            spectrum.get(i).add(0, (int)(instrumentValues.get(i)*h));
        }

        for (int i = 0; i < 8; i ++) {
            fill(getColor(i));
            // noFill();
            stroke(getColor(i));

            // left top <-
            beginShape();
            for (int j = 0; j < spectrum.get(i*2).size(); j ++) {
                vertex((width/2)-j*speed, ((height/16)+(height/8)*(i))+spectrum.get(i*2).get(j));
            }

            // left bottom ->
            for (int j = spectrum.get((i*2)+1).size()-1; j >= 0; j --) {
                vertex((width/2)-j*speed, ((height/16)+(height/8)*(i))-spectrum.get((i*2)+1).get(j));
            }
            endShape();

            // right top ->
            beginShape();
            for (int j = 0; j < spectrum.get(i*2).size(); j ++) {
                vertex((width/2)+j*speed, ((height/16)+(height/8)*(i))+spectrum.get(i*2).get(j));
            }

            // right bottom <-
            for (int j = spectrum.get((i*2)+1).size()-1; j >= 0; j --) {
                vertex((width/2)+j*speed, ((height/16)+(height/8)*(i))-spectrum.get((i*2)+1).get(j));
            }
            endShape();
        }

    }

}