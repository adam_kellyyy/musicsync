public class Speakers extends Graphic {

    int rotate = 0;
    int levels[] = new int[16];
    private ShootingStars stars = new ShootingStars(2, 0.001, 100, 2);


    public void render(String activeGraphics) {
        pushMatrix();

        pushMatrix();

        translate(0, 0, -6000);
        rotateX(PI/2);
        stars.render(null);
        
        popMatrix();

        translate(width/2, height/2, -500);
        noFill();

        rotateY(rotate*PI/1000);
        rotate++;

        stroke(255, 0, 0);
        noFill();
        strokeWeight(2);
        
        speaker(500);
        speaker(-500);

        popMatrix();

    }

    private void speaker(int pos) {
        
        int bassAverage = 0;
        for (int i = 0; i < 8; i ++) {
            bassAverage += freqBandPeak.get(i);
        }

        int trebleAverage = 0;
        for (int i = 0; i < 8; i ++) {
            trebleAverage += freqBandPeak.get(i+8);;
        }

        bassAverage = bassAverage/8;
        trebleAverage = trebleAverage/8;

        pushMatrix();
            translate(pos, 0, 0);
            box(400, 800, 600);

            pushMatrix();

                translate(0, 100, 300);
                circle(0, 0, 310);

                pushMatrix();
                    translate(0, 0, bassAverage);
                    circle(0, 0, 300);
                    circle(0, 0, 110);

                    pushMatrix();
                        translate(0, 0, (int)(-(bassAverage/2)));
                        circle(0, 0, 100);
                    popMatrix();
                popMatrix();

                

                pushMatrix();
                    translate(0, -350, 0);

                    pushMatrix();
                        translate(0, 0, trebleAverage);
                        circle(0, 0, 120);
                        circle(0, 0, 100);
                    popMatrix();

                    circle(0, 0, 90);
                popMatrix();

                

            popMatrix();
        popMatrix();
    }

}