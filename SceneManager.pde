public class SceneManager {

    private int currentScene = 0;
    private ArrayList<Graphic> graphics = new ArrayList<Graphic>();

    public void render() {
        background(0);
        String s = "";

        for (Graphic g : graphics) {
            if (g.isActive()) {
                s += g.getClass();
            }
        }

        for (Graphic g : graphics) {
            if (g.isActive()) {
                g.render(s.toLowerCase());
            }
        }
    }

    public void add(Graphic g) {
        graphics.add(g);
    }

    public void add(Graphic g, boolean active) {
        graphics.add(g);
        g.toggle();
    }

    public void keyNext() {
        currentScene ++;
        if (currentScene >= graphics.size()) {
            currentScene = 0;
        }
    }

    public void keyPrevious() {
        currentScene --;
        if (currentScene < 0) {
            currentScene = graphics.size() - 1;
        }
    }

    public void toggleCurrent() {
        graphics.get(currentScene).toggle();
    }

    public String toString() {
        String s = "";
        for (Graphic g : graphics) {
            s = s + g.toString() + "\n";
        }

        return s;
    }

    public void toggleScene(int scene) {
        graphics.get(scene).toggle();
    }
    
    public void setScene(int scene, boolean value) {
        if (scene < graphics.size()) {
            if (graphics.get(scene).isActive() != value) {
                graphics.get(scene).toggle();
            }
        }
        
    }

    public void setRed(int val) {
        for (Graphic g : graphics) {
            g.setRed(val);
        }
    }

    public void setGreen(int val) {
        for (Graphic g : graphics) {
            g.setGreen(val);
        }
    }

    public void setBlue(int val) {
        for (Graphic g : graphics) {
            g.setBlue(val);
        }
    }

    public boolean hasActive() {
        for (Graphic g : graphics) {
            if (g.isActive()) {
                return true;
            }
        }

        return false;
    }
}