public class City extends Graphic {
    int windowH = 25;
    int windowW = 10;
    float rotate = PI;
    float rotateRate = 0.002;
    int gapW = 20;
    int gapH = 20;
    ArrayList<Building> buildings = new ArrayList<Building>();
    ArrayList<Integer> mono = new ArrayList<Integer>();

    public City() {
        pushMatrix();

        for (int i = 0; i < 8; i ++) {
            mono.add(0);
        }

        Building building;
        building = new Building(30, 8, 0, 0, 0, 0);
        buildings.add(building);
        building = new Building(14, 3, 0, -1200, 0, 0);
        buildings.add(building);
        building = new Building(9, 1, 0, -1800, 0, 0);
        buildings.add(building);
        building = new Building(5, 15, 0, 0, 800, 0);
        buildings.add(building);
        building = new Building(6, 7, -800, 0, 800, 1);
        buildings.add(building);
        building = new Building(6, 7, 0, 0, -800, 1);
        buildings.add(building);
        building = new Building(4, 12, 800, 0, -600, 1);
        buildings.add(building);
        building = new Building(4, 2, -800, 0, -600, 1);
        buildings.add(building);
        building = new Building(4, 2, -700, 0, -400, 2);
        buildings.add(building);
        building = new Building(4, 2, -800, 0, -200, 2);
        buildings.add(building);
        building = new Building(8, 3, 0, 0, -400, 2);
        buildings.add(building);
        building = new Building(7, 3, 400, 0, -100, 3);
        buildings.add(building);
        building = new Building(9, 3, -500, 0, -400, 3);
        buildings.add(building);
        building = new Building(12, 4, -400, 0, -100, 3);
        buildings.add(building);
        building = new Building(5, 3, 600, 0, 200, 4);
        buildings.add(building);
        building = new Building(6, 3, 800, 0, 400, 4);
        buildings.add(building);
        building = new Building(3, 1, -300, 0, -800, 4);
        buildings.add(building);
        building = new Building(3, 1, -400, 0, -700, 5);
        buildings.add(building);
        building = new Building(3, 2, -600, 0, 100, 5);
        buildings.add(building);
        building = new Building(4, 1, -500, 0, 200, 5);
        buildings.add(building);
        building = new Building(3, 1, 300, 0, -600, 6);
        buildings.add(building);
        building = new Building(3, 1, 200, 0, -400, 6);
        buildings.add(building);
        building = new Building(3, 1, 700, 0, -100, 7);
        buildings.add(building);
        building = new Building(3, 1, 600, 0, 0, 7);
        buildings.add(building);
        building = new Building(3, 1, 500, 0, 700, 7);
        buildings.add(building);
        popMatrix();
        
    }

    public void render(String activeGraphics) {
        pushMatrix();
        

        for (int i = 0; i < 8; i ++) {
            mono.set(i, (freqBandPeak.get(i*2)+freqBandPeak.get((i*2)+1))/2);
        }

        rotate += rotateRate;
        if (rotate > 2*PI) {
            rotate = rotate - 2*PI;
        }

        noFill();
        pushMatrix();
        translate(width/2, 1000+height/2, -2000);
        


        pushMatrix();
        rotateY(rotate);
        pushMatrix();
        rotateX(PI/2);
        stroke(255);
        translate(-100*20, -100*20, 0);
        for (int i = 0; i < 40; i ++) {
            for (int j = 0; j < 40; j ++) {
                rect(100*j, 100*i, 100, 100);
            }
        }
        popMatrix();
        

        for (Building building : buildings) {
            building.draw();
        }
        popMatrix();

        
        popMatrix();
        popMatrix();
    }

    public class Building {
        int h;
        int w;
        int pixelH;
        int pixelW;
        int x;
        int y;
        int z;
        int cc;
        int midiChannel;
        int colour;

        public Building(int h, int w, int x, int y, int z, int midiChannel) {
            this.pixelH = (windowH+gapH)*h + gapH;
            this.h = h;
            this.pixelW = (windowW+gapW)*w + gapW;
            this.w = w;
            this.x = x;
            this.y = y;
            this.z = z;
            this.midiChannel = midiChannel;
            this.colour = midiChannel;
        }

        public void draw() {
            this.cc = mono.get(midiChannel);
            stroke(150, 0, 255);
            strokeWeight(2);
            fill(0);
            pushMatrix();
            translate(x, y, z);
            pushMatrix();
            translate(0, -(pixelH/2), 0);
            box(pixelW, pixelH, pixelW);
            popMatrix();
            

            fill(getColor(colour));
            noStroke();
            pushMatrix();
            translate(0, (-pixelH*cc/255.0), 0);
            box(pixelW+1, 2*pixelH*cc/255.0, pixelW+1);
            
            popMatrix();

            drawWall(0);
            drawWall(PI/2);
            drawWall(PI);
            drawWall(3*PI/2);
            popMatrix();
            
        }

        public void drawWall(float r) {
            fill(0);

            pushMatrix();
            translate(0, -pixelH, 0);
            rotateY(r);
            translate((pixelW/2)+2, 0, pixelW/2);
            rotateY(PI/2);
            noStroke();

            for (int i = 0; i < h; i ++) {
                pushMatrix();


                translate(0, (2*windowH*i), 0);
                rect(0, 0, pixelW, gapH);

                popMatrix();
            }

            pushMatrix();


            translate(0, pixelH-gapH*2, 0);
            rect(0, 0, pixelW, gapH*2);

            popMatrix();

            for (int i = 0; i < (w*2)+1; i ++) {

                if (i%2==1) {
                    continue;
                }

                pushMatrix();

                translate(((float)pixelW/((w*2)+1))*i, 0, 0);
                rect(0, 0, ((float)pixelW/((w*2)+1)), pixelH);

                popMatrix();
            }

            popMatrix();
        }

        public void setCC(int cc) {
            this.cc = cc;
        }

    }

}