public class Pinball extends Graphic {

    private int hammerHeight;
    private int hammerWidth;
    private float hammerInertia = 1;
    private float ballGravity = 0.1;
    private float ballBounciness = 0.6;
    private float ballClearance = -20;
    private float hammerWeight = 20;

    private ArrayList<Hammer> hammers = new ArrayList();
    private ArrayList<Ball> balls = new ArrayList();

    Ball myBall;

    

    public Pinball() {
        // hammers.add(new Hammer(800, 800, 800, 20, PI, 100));
        FloatPair point = rotatePoint(width/2, (height/2)+400, width/2, height/2, PI/8);
        for (int i = 0; i < 8; i ++) {
            float rotation = (float) i;
            FloatPair rotatedPoint = rotatePoint(point.getF1(), point.getF2(), width/2, height/2, rotation*2*PI/8);
            hammers.add(new Hammer(rotatedPoint.getF1(), rotatedPoint.getF2(), 350, 10, PI+PI/8 + rotation*2*PI/8, 100));
        }



        myBall = new Ball((width/2) + 200, 400, 10);
        balls.add(myBall);
    }

    public void render(String activeGraphics) {

        for (int i = 0; i < hammers.size(); i ++) {
            Hammer hammer = hammers.get(i);
            hammer.draw();
            hammer.collide(balls);
        }

        for (int i = 0; i < balls.size(); i ++) {
            Ball ball = balls.get(i);
            ball.draw();
            ball.update();
        }

        for (int i = 0; i < hammers.size(); i ++) {
            Hammer hammer = hammers.get(i);
            hammer.update(freqBandPeak.get(i));
        }

        

    }

    private class Ball {
        private float x;
        private float y;
        private float initialX;
        private float initialY;
        private float xVel;
        private float yVel;
        private float radius;

        public Ball(float x, float y, float radius) {
            this.x = x;
            this.y = y;
            this.initialX = this.x;
            this.initialY = this.y;
            this.radius = radius;
        }

        public Ball(float x, float y, float xVel, float yVel, float radius) {
            this.x = x;
            this.y = y;
            this.initialX = this.x;
            this.initialY = this.y;
            this.xVel = xVel;
            this.yVel = yVel;
            this.radius = radius;
        }

        public void draw() {
            circle(x, y, radius*2);
        }

        public void update() {

            this.x += this.xVel;
            this.y += this.yVel;

            this.yVel += ballGravity;

            if (y > height) {
                
                x = initialX;
                y = initialY;
            }

        }

        public float getX() {
            return x;
        }

        public float getY() {
            return y;
        }

        public float getRadius() {
            return radius;
        }

        public void collision(float angle, float velocity) {
            FloatPair flatVelocities = rotatePoint(xVel, yVel, 0, 0, -angle);
            FloatPair newFlatVelocities = new FloatPair(flatVelocities.getF1(), -flatVelocities.getF2());
            FloatPair newVelocities = rotatePoint(newFlatVelocities.getF1(), newFlatVelocities.getF2(), 0, 0, angle);
            FloatPair impartedVelocity = rotatePoint(0, velocity, 0, 0, angle + PI);
            // Pair<Float, Float> impartedVelocity = new Pair(0.0, 0.0);

            // make it so that the hammer cant impart velocity in the same direction the ball is travelling
 
            this.xVel = (newVelocities.getF1()*ballBounciness) - impartedVelocity.getF1();
            this.yVel = (newVelocities.getF2()*ballBounciness) - impartedVelocity.getF2();

            // clearance
            this.x += xVel;
            this.y += yVel;

        }

        public void setX(float x) {
            this.x = x;
        }

        public void setY(float y) {
            this.y = y;
        }

        public void setPos(FloatPair pos) {
            this.x = pos.getF1();
            this.y = pos.getF2();
        }

    }

    private class Hammer {

        private float x;
        private float y;
        private float originalY;
        private float rotation;
        private float h;
        private float w;
        private float travel;
        private float position = 0;
        private float velocity = 0;

        public Hammer(float x, float y, float w, float h, float rotation, float travel) {
            this.x = x;
            this.y = y;
            this.originalY = y;
            this.rotation = rotation;
            this.h = h;
            this.w = w;
            this.travel = travel;
            
        }

        public void draw() {
            push();
            translate(x, y, 0);
            rotateZ(rotation);
            rect(-w/2, position-(h/2), w, h);
            pop();
            
        }

        public void update(int value) {
            if (value > 90) {
                velocity += 100.0/(float)hammerWeight;
            }

            velocity -= hammerInertia;

            position += velocity;

            if (position > travel) {
                velocity = 0;
                position = travel;
            } else if (position < 0) {
                velocity = 0;
                position = 0;
            }

        }

        private float getY() {
            return y + rotatePoint(0, position, 0, 0, rotation).getF2();
        }

        private float getX() {
            return x + rotatePoint(0, position, 0, 0, rotation).getF1();
        }

        public void collide(ArrayList<Ball> balls) {

            for (Ball ball : balls) {

                FloatPair rotatedBall = rotatePoint(ball.getX(), ball.getY(), this.getX(), this.getY(), -rotation);

                float ballDistanceX = abs(rotatedBall.getF1() - this.getX());
                float ballDistanceY = abs(rotatedBall.getF2() - this.getY());

                if (ballDistanceX > (w/2 + ball.getRadius())) { return; }
                if (ballDistanceY > (h/2 + ball.getRadius())) { return; }

                if (ballDistanceX <= (w/2)) {
                    if (rotatedBall.getF2() < this.getY()) {
                        FloatPair newRotatedBallPos = new FloatPair(rotatedBall.getF1(), this.getY() - this.h/2 - ball.getRadius());
                        ball.setPos(rotatePoint(newRotatedBallPos.getF1(), newRotatedBallPos.getF2(), this.getX(), this.getY(), rotation));
                        
                        ball.collision(rotation, velocity);
                        return;
                    } else {
                        FloatPair newRotatedBallPos = new FloatPair(rotatedBall.getF1(), this.getY() + this.h/2 + ball.getRadius());
                        ball.setPos(rotatePoint(newRotatedBallPos.getF1(), newRotatedBallPos.getF2(), this.getX(), this.getY(), rotation));

                        ball.collision(rotation + PI, velocity);
                        return;
                    }
                }

                if (ballDistanceY <= (h/2)) {
                    if (rotatedBall.getF1() < this.getX()) {
                        FloatPair newRotatedBallPos = new FloatPair(this.getX() - this.w/2 - ball.getRadius(), rotatedBall.getF2());
                        ball.setPos(rotatePoint(newRotatedBallPos.getF1(), newRotatedBallPos.getF2(), this.getX(), this.getY(), rotation));

                        ball.collision(rotation - PI/2, velocity);
                        return;
                    } else {
                        FloatPair newRotatedBallPos = new FloatPair(this.getX() + this.w/2 + ball.getRadius(), rotatedBall.getF2());
                        ball.setPos(rotatePoint(newRotatedBallPos.getF1(), newRotatedBallPos.getF2(), this.getX(), this.getY(), rotation));

                        ball.collision(rotation + PI/2, velocity);
                        return;
                    }
                }

                float cornerDistance_sq = pow((ballDistanceX - w/2), 2) +
                                    pow((ballDistanceY - h/2), 2);

                if (cornerDistance_sq <= (pow(ball.getRadius(), 2))) {
                    if (rotatedBall.getF1() < this.getX() && rotatedBall.getF2() < this.getY()) {
                        // top left
                        ball.collision(rotation - PI/4, velocity);
                        // System.out.println("top left");
                    } else if (rotatedBall.getF1() < this.getX() && rotatedBall.getF2() > this.getY()) {
                        // bottom left
                        ball.collision(rotation - 3*PI/4, velocity);
                        // System.out.println("bottom left");
                    } else if (rotatedBall.getF1() > this.getX() && rotatedBall.getF2() < this.getY()) {
                        // top right
                        ball.collision(rotation + PI/4, velocity);
                        // System.out.println("top right");
                    } else {
                        // bottom right
                        ball.collision(rotation + 3*PI/4, velocity);
                        // System.out.println("bottom right");
                    }
                }


                // if (dist(ball.getX(), ball.getY(), x, y) < sqrt(pow(w/2, 2) + pow(h/2, 2)) + ball.getRadius()) {

                //     Pair<Float, Float> A = rotatePoint(x - w/2, y - h/2, x, y, rotation);
                //     Pair<Float, Float> B = rotatePoint(x + w/2, y - h/2, x, y, rotation);
                //     Pair<Float, Float> C = rotatePoint(x + w/2, y + h/2, x, y, rotation);
                //     Pair<Float, Float> D = rotatePoint(x - w/2, y + h/2, x, y, rotation);

                //     float distance = pointLineDist(A, B, ball.getX(), ball.getY());
                //     String closest = "N";

                //     if (pointLineDist(B, C, ball.getX(), ball.getY()) < distance) {
                //         closest = "E";
                //         distance = pointLineDist(B, C, ball.getX(), ball.getY());
                //     }

                //     if (pointLineDist(C, D, ball.getX(), ball.getY()) < distance) {
                //         closest = "S";
                //         distance = pointLineDist(B, C, ball.getX(), ball.getY());
                //     }

                //     if (pointLineDist(D, A, ball.getX(), ball.getY()) < distance) {
                //         closest = "W";
                //         distance = pointLineDist(B, C, ball.getX(), ball.getY());
                //     }

                //     String collision = "";

                //     if (closest.equals("N") && pointLineDist(A, B, ball.getX(), ball.getY()) < ball.getRadius()) {
                //         collision = "N";
                //         ball.collision(rotation);
                //     } else if (closest.equals("E") && pointLineDist(B, C, ball.getX(), ball.getY()) < ball.getRadius()) {
                //         collision = "E";
                //         ball.collision(rotation + PI/4);
                //     } else if (closest.equals("S") && pointLineDist(C, D, ball.getX(), ball.getY()) < ball.getRadius()) {
                //         collision = "S";
                //         ball.collision(rotation + PI/2);
                //     } else if (closest.equals("W") && pointLineDist(D, A, ball.getX(), ball.getY()) < ball.getRadius()) {
                //         collision = "W";
                //         ball.collision(rotation + 3*PI/4);
                //     }

                // }


            }
        }

        
    } 
        
}

public class FloatPair {
    private Float f1;
    private Float f2;

    public FloatPair(Float f1, Float f2) {
        this.f1 = f1;
        this.f2 = f2;
    }

    public void setF1(Float f1) {
        this.f1 = f1;
    }

    public void setF2(Float f2) {
        this.f2 = f2;
    }

    public Float getF1() {
        return f1;
    }

    public Float getF2() {
        return f2;
    }
}

public FloatPair rotatePoint(float px, float py, float cx, float cy, float angle) {
    float s = sin(angle);
    float c = cos(angle);

    // translate point back to origin:
    px -= cx;
    py -= cy;

    // rotate point
    float xnew = px * c - py * s;
    float ynew = px * s + py * c;

    // translate point back:
    px = xnew + cx;
    py = ynew + cy;
    return new FloatPair(px, py);
}

public float pointLineDist(FloatPair A, FloatPair B, float cx, float cy) {
    return 
    abs((B.getF1() - A.getF1()) * (cy - A.getF2()) - (B.getF2() - A.getF2()) * (cx - A.getF1()))
    /
    sqrt(pow(B.getF1() - A.getF1(), 2) + pow(B.getF2() - A.getF2(), 2));
}