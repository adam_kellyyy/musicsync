public class GameOfLife extends Graphic {
    private int boardScale = 8;
    private color[][] board;
    private int[][] pixelLife;
    private int[][] boardAdj;
    private int boardHeight;
    private int boardWidth;
    private int delay = 0;
    private int delayTimer = 1;
    private int noiseThreshold = 120;
    private int pixelFrequency = 20;
    private int removalRate = 20;
    private int pixelCount = 0;
    private int minPixels = 50;
    private int red = 0;
    private int green = 0;
    private int blue = 0;
    
    public GameOfLife() {
        boardHeight = height/boardScale;
        boardWidth = width/boardScale;
        board = new color[boardHeight][boardWidth];
        boardAdj = new int[boardHeight][boardWidth];
        pixelLife = new int[boardHeight][boardWidth];
        boolean last = false;
        for (int y = 0; y < boardHeight; y ++) {
            for (int x = 0; x < boardWidth; x ++) {
                if (random(0, 100) < 5) {
                    board[y][x] = color(0, 255, 0);
                    pixelLife[y][x] = 5;
                } else {
                    board[y][x] = BLACK;
                }
            }
        }
    }

    public void render(String activeGraphics) {

        noFill();
        stroke(0);

        if (delay < delayTimer) {
            delay++;
        } else { 
            delay = 0;

            for (int i = 0; i < 16; i ++) {
                if (freqBandValues.get(i) > noiseThreshold) {
                    for (int j = 0; j < pixelFrequency; j ++) {
                        board[(int)random(0, boardHeight)][(int)random(0, boardWidth)] = color(red, green, blue);
                    }
                } 
                else if (freqBandValues.get(i) < 10 && pixelCount > minPixels){
                    for (int j = 0; j < removalRate; j ++) {
                        board[(int)random(0, boardHeight)][(int)random(0, boardWidth)] = BLACK;
                    }
                }
            }

            for (int y = 0; y < boardHeight; y ++) {
                for (int x = 0; x < boardWidth; x ++) {
                    int num = 0;
                    int[][] moves = {{-1,0}, {-1, -1}, {0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1}, {-1, 1}};
                    for (int l = 0; l < 8; ++l) {
                        int tx = x + moves[l][0];
                        int ty = y + moves[l][1];
                        if (tx < 0 || tx >= boardWidth || ty < 0 || ty >= boardHeight) {
                        continue;
                        }
                        if (board[ty][tx]!= BLACK) {
                            num ++;
                        }
                    }
                    boardAdj[y][x] = num;
                }
            }

            pixelCount = 0;

            for (int y = 0; y < boardHeight; y ++) {
                for (int x = 0; x < boardWidth; x ++) {
                    if (board[y][x] != BLACK) {
                        pixelCount ++;
                        if (boardAdj[y][x] < 2) {
                            board[y][x] = BLACK;
                        } else if (boardAdj[y][x] > 3) {
                            board[y][x] = BLACK;
                        }
                    } else {
                        if (boardAdj[y][x] == 3) {
                            board[y][x] = color(red, green, blue);
                        }
                    }
                }
            }

            for (int y = 0; y < boardHeight; y ++) {
                for (int x = 0; x < boardWidth; x ++) {
                    if (board[y][x] == BLACK) {
                        if (pixelLife[y][x] > 0) {
                            pixelLife[y][x] -= 1;
                        }
                    } else {
                        pixelLife[y][x] = 3;
                    }
                }
            }
        }

        for (int y = 0; y < boardHeight; y ++) {
            for (int x = 0; x < boardWidth; x ++) {
                
                if (board[y][x] != BLACK) {
                    fill(board[y][x]);
                    rect(x*boardScale, y*boardScale, boardScale, boardScale);

                }
                // circle((x*boardScale)+(boardScale/2), (y*boardScale)+(boardScale/2), boardScale);
            }
        }
    }

    public void setRed (int val) {
        red = val;
    }

    public void setGreen (int val) {
        green = val;
    }

    public void setBlue (int val) {
        blue = val;
    }

}