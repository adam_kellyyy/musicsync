public class Rubix extends Graphic {
    private int rubixNum = 8;
    private int rubixScale = 40;
    private float rubixCCScale = 1.5;
    private int rubixRotate = 0;
    private int rubixRotationRate = 720;
    private int rubixLevels[] = new int[64];
    private int[] rubixAssignments = {4,28,52,47,54,15,45,48,37,13,6,33,60,64,26,55,16,23,38,59,2,7,39,36,12,62,35,50,53,42,49,17,18,5,58,24,10,19,29,8,63,43,30,27,61,32,11,46,51,14,34,22,41,57,44,25,21,20,1,9,31,56,3,40};

    public void render(String activeGraphics) {

        strokeWeight(2);

        rubixRotate ++;
        if (rubixRotate*PI/rubixRotationRate > 2*PI) {
            rubixRotate = 0;
        }
        for (int i = 0; i < 64; i ++) {
            rubixLevels[rubixAssignments[i]-1] = instrumentPeak.get(i/4)/2;
        }

        pushMatrix();

        translate(0, 0, 300);
        
        for (int k = 0; k < 6; k ++) {

            switch (k) {
                case 0:

                    stroke(255,0,0);
                    noFill();
                    translate(width/2, height/2, 0);
                    rotateY(rubixRotate*PI/rubixRotationRate);
                    translate(-(rubixNum*rubixScale)/2, -(rubixNum*rubixScale)/2, (rubixNum*rubixScale)/2);
                    break;
                case 1:
                    stroke(0,255,0);
                    pushMatrix();
                    translate(0, rubixNum*rubixScale, 0);
                    rotateX(-PI/2);
                    break;
                case 2:
                    stroke(0,0,255);
                    pushMatrix();
                    translate(0, 0, -rubixNum*rubixScale);
                    rotateY(-PI/2);
                    break;
                case 3:
                    stroke(255,255,0);
                    pushMatrix();
                    translate(rubixNum*rubixScale, 0, 0);
                    rotateY(PI/2);
                    break;
                case 4:
                    stroke(255,0,255);
                    pushMatrix();
                    translate(0, 0, -rubixNum*rubixScale);
                    rotateX(PI/2);
                    break;
                case 5:
                    stroke(0,255,255);
                    pushMatrix();
                    translate(0, rubixNum*rubixScale, -rubixNum*rubixScale);
                    rotateX(PI);
                    break;
            }

            for (int i = 0; i < rubixNum; i ++) {
                for (int j = 0; j < rubixNum; j ++) {
                    
                    beginShape();
                    vertex((i*rubixScale), (j*rubixScale), 0);
                    vertex((i*rubixScale), (j*rubixScale), 0+rubixLevels[(i*8)+j]*rubixCCScale);
                    vertex((i*rubixScale)+rubixScale, (j*rubixScale), 0+rubixLevels[(i*8)+j]*rubixCCScale);
                    vertex((i*rubixScale)+rubixScale, (j*rubixScale), 0);
                    vertex((i*rubixScale), (j*rubixScale), 0);

                    vertex((i*rubixScale)+rubixScale, (j*rubixScale), 0);
                    vertex((i*rubixScale)+rubixScale, (j*rubixScale), 0+rubixLevels[(i*8)+j]*rubixCCScale);
                    vertex((i*rubixScale)+rubixScale, (j*rubixScale)+rubixScale, 0+rubixLevels[(i*8)+j]*rubixCCScale);
                    vertex((i*rubixScale)+rubixScale, (j*rubixScale)+rubixScale, 0);
                    vertex((i*rubixScale)+rubixScale, (j*rubixScale), 0);

                    vertex((i*rubixScale)+rubixScale, (j*rubixScale)+rubixScale, 0);
                    vertex((i*rubixScale)+rubixScale, (j*rubixScale)+rubixScale, 0+rubixLevels[(i*8)+j]*rubixCCScale);
                    vertex((i*rubixScale), (j*rubixScale)+rubixScale, 0+rubixLevels[(i*8)+j]*rubixCCScale);
                    vertex((i*rubixScale), (j*rubixScale)+rubixScale, 0);
                    vertex((i*rubixScale)+rubixScale, (j*rubixScale)+rubixScale, 0);

                    vertex((i*rubixScale), (j*rubixScale)+rubixScale, 0);
                    vertex((i*rubixScale), (j*rubixScale)+rubixScale, 0+rubixLevels[(i*8)+j]*rubixCCScale);
                    vertex((i*rubixScale), (j*rubixScale), 0+rubixLevels[(i*8)+j]*rubixCCScale);
                    vertex((i*rubixScale), (j*rubixScale), 0);
                    vertex((i*rubixScale), (j*rubixScale)+rubixScale, 0);
                    
                    endShape();
                }
            }
            if (k != 0) {
                popMatrix();
            }
        }

        popMatrix();

        
    }
}