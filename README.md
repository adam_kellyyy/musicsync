This project is a graphics library that responds to MIDI data, intended to be used for visual accompaniment during live musical performance.

<img src="https://i.imgur.com/cpoocKM.png" alt="" style="zoom:50%;" />
<img src="https://i.imgur.com/8UOTlsZ.png" alt="" style="zoom:50%;" />
<img src="https://i.imgur.com/z8YHpJn.png" alt="" style="zoom:50%;" />
<img src="https://i.imgur.com/WNlp5EL.png" alt="" style="zoom:50%;" />
<img src="https://i.imgur.com/eBt85xS.png" alt="" style="zoom:50%;" />
<img src="https://i.imgur.com/LhP0EWd.png" alt="" style="zoom:50%;" />
<img src="https://i.imgur.com/Gogvv9i.png" alt="" style="zoom:50%;" />
<img src="https://i.imgur.com/y93dzj3.png" alt="" style="zoom:50%;" />
<img src="https://i.imgur.com/AEaIKgX.png" alt="" style="zoom:50%;" />
<img src="https://i.imgur.com/ou5xvD8.png" alt="" style="zoom:50%;" />
