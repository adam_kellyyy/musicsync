public class ShootingStars extends Graphic {

    private float shootingStarFrequency = 5;
    private float shootingStarAgeing = 0.1;
    private float shootingStarSpeed = 5;
    private float shootingStarMaxSpeed = 100;
    private float shootingStarMaxSize = 10;
    private float shootingStarDirection = 0;

    private ArrayList<ShootingStar> stars = new ArrayList<>();

    public ShootingStars() {

    }

    public ShootingStars(float shootingStarFrequency, float shootingStarAgeing, float shootingStarSpeed, float shootingStarMaxSize) {
        this.shootingStarFrequency = shootingStarFrequency;
        this.shootingStarAgeing = shootingStarAgeing;
        this.shootingStarSpeed = shootingStarSpeed;
        this.shootingStarMaxSize = shootingStarMaxSize;

    }

    private class ShootingStar {
        private float x;
        private float y;
        private float z;
        private float startingY;
        private float size;
        private float startingSize;
        private float speed;
        private float length;

        public ShootingStar(float x, float y, float z, float size, float speed) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.startingY = y;
            this.size = size;
            this.startingSize = size;
            this.speed = speed;
            this.length = size * random(50, 400);
        }

        public void draw() {
            pushMatrix();
            translate(0, height/2, z-(width/2));
            stroke(255.0*(size/startingSize));     
            strokeWeight(size);
            line(x, y, x, startingY);
            popMatrix();
        }

        public boolean update() {

            y += speed;
            if (y > length) {
                startingY += speed;
            }

            size = size - shootingStarAgeing;
            if (size < 0) {
                return true;
            }

            return false;

        }

    }

    public void render(String activeGraphics) {

        int cctotal = 0;
        for (int i = 0; i < 4; i ++) {
            cctotal += freqBandPeak.get(i);
        }

        cctotal = cctotal/4;


        if (random(0, 255) + (shootingStarFrequency*cctotal) > 255) {
            int x = (int)random(0, width);
            int z = (int)random(0, height);
            float size = random(0, shootingStarMaxSize);
            float speed = random(shootingStarSpeed-50, shootingStarSpeed);
            stars.add(new ShootingStar(x, 0, z, size, speed));
        }

        ArrayList<ShootingStar> removals = new ArrayList<>();

        for (ShootingStar star : stars) {
            star.draw();
            if (star.update()) {
                removals.add(star);
            }
        }

        for (ShootingStar star : removals) {
            stars.remove(star);
        }
    }

}
    