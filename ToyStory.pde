public class ToyStory extends Graphic {

    private int speed = 20;
    private int sensitivity = 40;
    private int cutoff = 5000;
    private int radius = 500;
    private int thickness = 50;
    private int spacing = 100;

    private ArrayList<ArrayList<Event>> rails = new ArrayList();

    public ToyStory() {
        
        for (int i = 0; i < 16; i ++) {
            rails.add(new ArrayList<Event>());
        }
    }

    public void render(String activeGraphics) {

        for (int i = 0; i < 16; i ++) {

            // maintenance

            ArrayList<Event> rail = rails.get(i);

            if (rail.size() == 0) {
                if (instrumentPeak.get(i) >= sensitivity) {
                    rail.add(new Event());
                    rail.get(rail.size()-1).start();
                    rail.get(rail.size()-1).iterate(speed);
                }
                continue;
            }

            if (rail.get(rail.size()-1).isRunning()) {
                if (instrumentPeak.get(i) < sensitivity) {
                    rail.get(rail.size()-1).stop();
                }
            } else {
                if (instrumentPeak.get(i) >= sensitivity) {
                    rail.add(new Event());
                    rail.get(rail.size()-1).start();

                }
            }

            for (int j = 0; j < rail.size(); j ++) {
                rail.get(j).iterate(speed);
                if (rail.get(j).getLocation() - rail.get(j).getLength() > cutoff) {
                    rail.remove(j);
                    j--;
                }
            }

            // rendering

            push();


            // centre focus
            translate(width/2, (height/2), 0);

            // rotate for sides of octagon
            rotateZ((i/2)*PI/4);

            // move up to first side of octagon
            translate(0, -radius, 0);

            // centre rectangle drawing
            translate(-thickness/2, 0, 0);
            // split up left and right channels
            if (i%2==0) {
                translate(-spacing/2, 0, 0);
            } else {
                translate(spacing/2, 0, 0);
            }

            rotateX(-PI/2);

            for (Event event : rail) {
                fill(255, 255, 255);

                rect(0, event.getLocation(), thickness, -event.getLength());
            }

            pop();
            
        }



    }

    private class Event {
        private int location = 0;
        private int length = 0;
        private boolean running = false;

        public int getLocation() {
            return location;
        }

        public int getLength() {
            return length;
        }

        public boolean isRunning() {
            return running;
        }

        public void start() {
            running = true;
        }

        public void stop() {
            running = false;
        }

        public void iterate(int speed) {
            location += speed;
            if (running == true) {
                length += speed;
            }
        }


    }

        
}