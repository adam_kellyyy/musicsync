int BLUE = 10;
int RED = 11;
int GREEN = 12;
int TEST = 13;

int blueValue = 255;
int greenValue = 255;
int redValue = 255;

void setup() {
    pinMode(TEST, OUTPUT);
    pinMode(BLUE, OUTPUT);
    pinMode(RED, OUTPUT);
    pinMode(GREEN, OUTPUT);

    usbMIDI.setHandleControlChange(myControlChange);

}

void loop() {
    usbMIDI.read();
}

void adjustBrightness(int pin, int brightness) {
    analogWrite(pin, brightness);
}

void myControlChange(byte chan, byte control, byte value) {

	if (chan == 1) {
		adjustBrightness(RED, value);
        adjustBrightness(TEST, value);
	} else if (chan == 2) {
        adjustBrightness(BLUE, value);
    } else if (chan == 3) {
        adjustBrightness(GREEN, value);
    }

    

}