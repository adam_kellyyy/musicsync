public class Waves extends Graphic {

	private int particleSize = 8;
	private int particleCornerRadius = 0;
	private int[] xLocations = {-300, -100, 100, 300, -300, -100, 100, 300, -300, -100, 100, 300, -300, -100, 100, 300};
	private int[] yLocations = {-300, -300, -300, -300, -100, -100, -100, -100, 100, 100, 100, 100, 300, 300, 300, 300};
	private int numSectors = 1024;
	private int particleIntensity = 2;
	private int noiseGate = 80;
	private float velocity = 4;
	private float collisionDistance = 10;
	private float collisionAge = 150;
	private int particleAge = 300;

	public ArrayList<Sector> sectors = new ArrayList<Sector>();

	private class Particle {
		private float xPos;
		private float yPos;
		private float direction;
		private float velocity;
		private color c;
		private int age;

		public Particle(float xPos, float yPos, float direction, float velocity, color c) {
			this.xPos = xPos;
			this.yPos = yPos;
			this.direction = direction;
			this.velocity = velocity;
			this.c = c;
			this.age = 0;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int f) {
			age = f;
		}

		public float getXPos() {
			return xPos;
		}

		public float getYPos() {
			return yPos;
		}

		public float getDirection() {
			return direction;
		}

		public float getVelocity() {
			return velocity;
		}

		public void setXPos(float f) {
			xPos = f;
		}

		public void setYPos(float f) {
			yPos = f;
		}

		public void setDirection(float f) {
			direction = f;
		}

		public void setVelocity(float f) {
			velocity = f;
		}

		public color getColor() {
			return c;
		}

		public void update() {
			xPos += velocity*cos(direction);
			yPos += velocity*sin(direction);
			age += 1;
		}

		public void render() {
			fill(c, ((float)(particleAge-this.age)/particleAge)*255.0);
			noStroke();
			// rect(xPos, yPos, particleSize, particleSize, particleCornerRadius, particleCornerRadius, particleCornerRadius, particleCornerRadius);
			circle(xPos, yPos, particleSize);
		}

	}

	private class Sector {
		private ArrayList<Particle> particles = new ArrayList<Particle>();

		private int xPos;
		private int yPos;

		public Sector(int xPos, int yPos) {
			this.xPos = xPos;
			this.yPos = yPos;
		}

		public int getXPos() {
			return xPos;
		}

		public int getYPos() {
			return yPos;
		}

		public ArrayList<Particle> getParticles() {
			return particles;
		}

		public void update() {

			for (int i = 0; i < particles.size(); i ++) {
				Particle pa = particles.get(i);
				for (int j = i + 1; j < particles.size(); j ++) {
					Particle pb = particles.get(j);

					if (pa.getAge() < collisionAge && pa.getColor() != pb.getColor() && dist(pa.getXPos(), pa.getYPos(), pb.getXPos(), pb.getYPos()) < collisionDistance) {

						float axVel = pa.getVelocity()*cos(pa.getDirection());
						float ayVel = pa.getVelocity()*sin(pa.getDirection());

						float bxVel = pb.getVelocity()*cos(pb.getDirection());
						float byVel = pb.getVelocity()*sin(pb.getDirection());

						float xAvg = (axVel + bxVel)/2.0;
						float yAvg = (ayVel + byVel)/2.0;

						pa.setVelocity(sqrt(pow(xAvg, 2) + pow(yAvg, 2)));
						pb.setVelocity(sqrt(pow(xAvg, 2) + pow(yAvg, 2)));
						pa.setDirection((pa.getDirection() + pb.getDirection())/2.0);
						pb.setDirection((pa.getDirection() + pb.getDirection())/2.0);
					}

				}
			}

			for (int i = 0; i < particles.size(); i ++) {
				Particle p = particles.get(i);
				p.update();

				if (p.getAge() > particleAge || p.getXPos() > width/2 || p.getXPos() < -width/2 || p.getYPos() > height/2 || p.getYPos() < -height/2) {
					particles.remove(i);
					i --;
				}
				
			}



		}

		public void render() {
			for (Particle p : particles) {
				p.render();
			}
		}

	}

	private void generateWaves() {
		for (int i = 0; i < 16; i ++) {
			if (instrumentValues.get(i) > noiseGate) {
				for (int j = 0; j < particleIntensity; j ++) {

					Particle p = new Particle((float)xLocations[i], (float)yLocations[i], random(0, 2*PI), velocity, getColor(i/2));
					sectors.get(0).getParticles().add(p);
				}
			}
		}
		
	}

	private void refreshSectorPopulations() {
		for (int i = 0; i < sectors.size(); i ++) {
			Sector s = sectors.get(i);
			for (int j = 0; j < s.getParticles().size(); j ++) {
				Particle p = s.getParticles().get(j);
				float dist = width*2.0;
				int ind = 0;
				for (int k = 0; k < sectors.size(); k ++) {
					if (dist(sectors.get(k).getXPos(), sectors.get(k).getYPos(), p.getXPos(), p.getYPos()) < dist) {
						dist = dist((float)sectors.get(k).getXPos(), (float)sectors.get(k).getYPos(), p.getXPos(), p.getYPos());
						ind = k;
					}
				}

				s.getParticles().remove(p);
				sectors.get(ind).getParticles().add(p);

			}
		}
	}

	public void render(String activeGraphics) {

		generateWaves();
		refreshSectorPopulations();
		pushMatrix();
        translate(width/2, height/2);
		for (Sector s : sectors) {
			s.render();
			s.update();
		}


		popMatrix();
	}

	public Waves() {

		Sector s;

		for (int i = 0; i < sqrt(numSectors); i ++) {
			for (int j = 0; j < sqrt(numSectors); j ++) {
				s = new Sector((int)((-width/2)+(i*width/sqrt(numSectors))), (int)((-height/2)+(j*height/sqrt(numSectors))));
				sectors.add(s);
			}
		}

	}

}

